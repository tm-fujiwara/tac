<?php
/**
 * メールフォーム処理分岐振り分けファイル
 * @author     Suguru Chiba @ Respect
 */

// 定義ファイルの呼び出し
require_once dirname(__FILE__) . '/./settings.php';

// コモンライブラリ呼び出し
require_once LIB_DIR . '/./Common.Lib.php';

// ユーティリティクラス呼び出し
require_once LIB_DIR . '/./Class.Util.php';

// ログクラス呼び出し
require_once LIB_DIR . '/./Class.Log.php';

// メール送信先などの設定ファイル呼び出し
require_once THIS_ROOT . '/./config.php';

if (DEBUG) {
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
} else {
	error_reporting(0);
	ini_set('display_errors', 'Off');
}

// クラスファイルの呼び出し
require_once(LIB_DIR . 'Class.MailForm.php');

$mailform = new MailForm(@$_POST);

$mailform->execute();