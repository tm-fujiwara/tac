<?php
/**
 * バリデーションルールを追加する関数
 */
function add_valid_rule($key, $type, $message=null) {
	global $setting_list;
	array_push($setting_list, array('key' => $key, 'type' => $type, 'message' => $message));
}

/**
 * YYYY年MM月DD日(W)の連想配列を返す
 * ○日から○ヶ月先までのプルダウンを作りたい場合にどうぞ
 * 
 * @arg $dalay=開始日を当日から何日ずらすか
 * @arg $during=何ヶ月先まで配列に入れ込むか
 * @author Suguru Chiba
 * @return Array
 */
function makeScheduleArray($delay=0, $during=3) {
	$weekday = array('日','月','火','水','木','金','土');
	// スタートする日付情報の取得
	$delay_date = $delay * 60 * 60 * 24;
	$now = getdate(time() + $delay_date);
	$month = $now['mon'];
	$year = $now['year'];
	$start_day = $now['mday'];
	$first_flag = true;
	$limit = $month+(int)$during;
	// 指定した月数分回す
	$arr = array();
	for ($i=$month; $i<$limit+1; $i++) {
		if ($month > 12) {
			// 年月のリフォーム
			$_year = $year + floor((int)$month / 12);
			$_month = $month;
			while($_month > 12) {
				$_month = $_month -12;
			}
		} else {
			$_year = $year;
			$_month = $month;
		}

		$date_count = date('t', mktime(0, 0, 0, $_month, 1, $year));
		if ($i == $limit) {
			$date_count = (int)$start_day;
		}
		$_start_day = ($first_flag) ? $start_day : 1;
		for ($j=$_start_day; $j<=$date_count; $j++) {
			$this_date = date('Y年m月d日',  mktime(0, 0, 0, $_month, $j, $_year));
			$now_weekday = '(' . $weekday[date('w',  mktime(0, 0, 0, $_month, $j, $_year))] . ')';
			$arr[$_year . '年' . sprintf('%02d', $_month) . '月'][$this_date . $now_weekday] = $this_date;
			$arr[$_year . '年' . sprintf('%02d', $_month) . '月'][$this_date . $now_weekday] .= $now_weekday;
		}
		$month++;
		$first_flag = false;
	}
	return $arr;
}