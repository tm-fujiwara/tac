<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="TAC税理士講座「税理士試験5科目合格祝賀会」ご予約ページです。">
<meta name="keywords" content="税理士,試験,5科目,合格,祝賀会">
<title>「税理士試験5科目合格祝賀会」ご予約</title>
<link href="../../css/zei/celebration_reserve.css" rel="stylesheet">
<script src="../../js/zei/celebration_reserve/common.js"></script>
</head>
<body id="error">
<div id="container">
	<header id="header" class="clearfix">
		<h1><a href="http://www.tac-school.co.jp/" target="_blank"><img src="../../images/zei/celebration_reserve/common/logo.png" width="271" height="32" alt="資格の学校TAC"></a></h1>
		<div class="hTtl"><img src="../../images/zei/celebration_reserve/common/imgtext01.png" width="336" height="215" alt="参加対象者 TAC税理士講座のコース※を受講され66回税理a士試験において5科目合格を達成された方 ※オプション講座や全a国公開模試のみの受講者は対象外となります。"></div>
	</header>

	<div class="mainVisual">
		<section id="pageTtl">
			<h2 class="h2Ttl"><img src="../../images/zei/celebration_reserve/index/h2_img.png" width="472" height="209" alt="平成28年度(第66回)税理士試験 5科目合格祝賀会 Web予約受付 登録期日 各地区の祝賀会実施日の3日前まで"></h2>
		</section>
	</div>

	<div id="main">
		<section id="tac">
			<h3 class="h3Ttl">TAC税理士講座受講生 5科目合格者の皆様へ</h3>
			<p>TACでは、5科目合格者の皆様のご健闘を称えるとともに合格を祝しまして、合格祝賀会を開催いたします。ご参加にあたっては、<span>下記期間</span>までに当ページにてお手続きください。後日メールにて祝賀会招待メールをお送りいたします。</p>
		</section>

		<section id="time">
			<div class="ttlBox">
				<h3 class="h3Ttl"><img src="../../images/zei/celebration_reserve/index/h3_img01.png" width="463" height="24" alt="●合格祝賀会実施日及びWeb予約期限"></h3>
				<p class="textLink"><a href="http://www.tac-school.co.jp/kouza_zeiri/zeiri_shukuga.html" target="_blank">祝賀会の詳細はコチラ</a></p>
			</div>
			<ul class="timeList clearfix">
				<li><img src="../../images/zei/celebration_reserve/index/imgtext02.png" width="182" height="235" alt="東京会場 2017年1/7(土) 受付17：30 開宴18：30 (16：30より特別セミナーを開演) 【Web予約期限】1/4(水)"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext03.png" width="182" height="235" alt="名古屋会場 2017年1/8(日) 受付17：30 開宴18：30 【Web予約期限】1/5(木)"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext04.png" width="182" height="235" alt="大阪会場 2017年1/15(日) 受付17：15 開宴18：30 【Web予約期限】1/12(木)"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext05.png" width="182" height="235" alt="広島会場 2017年1/15(日) 受付17：30 開宴18：00 【Web予約期限】1/12(木)"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext06.png" width="182" height="235" alt="福岡会場 2017年1/14(土) 受付15：30 開宴16：00 【Web予約期限】1/11(水)"></li>
			</ul>
			<p>※当Web出席登録ページの受付締切日以降に、祝賀会参加登録をご希望の方は、TACカスタマーセンター(0120-509-117)までご連絡ください。</p>
		</section>

		<section id="celebration">
			<h3 class="h3Ttl"><img src="../../images/zei/celebration_reserve/index/h3_img02.png" width="384" height="23" alt="●祝賀会ご参加までの3ステップ"></h3>
			<ul class="stepUl clearfix">
				<li><img src="../../images/zei/celebration_reserve/index/imgtext07.jpg" width="300" height="150" alt="1当ページでご予約 ※予約受付完了メールが届きます。"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext08.jpg" width="300" height="150" alt="2後日、祝賀会招待メールが届きます"></li>
				<li><img src="../../images/zei/celebration_reserve/index/imgtext09.jpg" width="300" height="150" alt="3祝賀会当日、受付窓口にて祝賀会招待メールをご提示 ※印刷したものか携帯電話等の画面表示"></li>
			</ul>
		</section>

		<section id="contact">
			<h3 class="contactH3"><img src="../../images/zei/celebration_reserve/index/contact_h3.png" width="178" height="25" alt="必要事項の入力"></h3>
			<p class="redNote">※すべての項目の入力が必要です。</p>
			<div class="contactBox">
				<form action="#contact" method="post" class="mailForm">
					<table class="formTable">
						<tbody>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>TAC会員番号（10桁）</th>
								<td>
									<input type="text" name="num" value="<!--{$num|escape}-->"><span class="note">（半角数字 0から始まる10桁）</span>
									<!--{if isset($errors.list.num)}-->
										<!--{foreach from=$errors.message.num.0 item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>氏名</th>
								<td>
									<span class="name">姓</span><input type="text" name="sei" value="<!--{$sei|escape}-->"><span class="name">名</span><input type="text" name="mei" value="<!--{$mei|escape}-->"><span class="note">（全角）</span>
									<!--{if isset($errors.list.sei)}-->
										<!--{foreach from=$errors.message.sei item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<!--{if isset($errors.list.mei)}-->
										<!--{foreach from=$errors.message.mei item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>フリガナ</th>
								<td>
									<span class="name">セイ</span><input type="text" name="sei_kana" value="<!--{$sei_kana|escape}-->"><span class="name">メイ</span><input type="text" name="mei_kana" value="<!--{$mei_kana|escape}-->"><span class="note">（全角カタカナ）</span>
									<!--{if isset($errors.list.sei_kana)}-->
										<!--{foreach from=$errors.message.sei_kana item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<!--{if isset($errors.list.mei_kana)}-->
										<!--{foreach from=$errors.message.mei_kana item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td>
									<input type="tel" name="tel" value="<!--{$tel|escape}-->"><span class="note">（半角数字 ハイフン不要 携帯可）</span>
									<!--{if isset($errors.list.tel)}-->
										<!--{foreach from=$errors.message.tel item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>メールアドレス</th>
								<td>
									<input type="email" name="email" value="<!--{$email|escape}-->"><span class="note">（半角英数字）</span>
									<!--{if isset($errors.list.email)}-->
										<!--{foreach from=$errors.message.email.0 item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>メールアドレス（確認用）</th>
								<td>
									<input type="email" name="email_confirm" value="<!--{$email_confirm|escape}-->"><span class="note">（半角英数字）</span>
									<!--{if isset($errors.list.email_confirm)}-->
										<!--{foreach from=$errors.message.email_confirm item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>祝賀会参加会場</th>
								<td>
									<!--{html_options name="cele_area" options=$form_elements.cele_area selected=$cele_area}-->
									<!--{if isset($errors.list.cele_area)}-->
										<!--{foreach from=$errors.message.cele_area item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>科目情報</th>
								<td>
									<div class="info info1">
										<p class="infoText">平成28年度第66回税理士試験受験地</p>
										<!--{html_options name="exam_area" options=$form_elements.exam_area selected=$exam_area}-->
										<!--{if isset($errors.list.exam_area)}-->
											<!--{foreach from=$errors.message.exam_area item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<div class="info info2">
										<p class="infoText">平成28年度第66回税理士試験受験番号（5桁）</p>
										<input type="text" name="exam_num" value="<!--{$exam_num|escape}-->"><span class="note">（半角数字）</span>
										<!--{if isset($errors.list.exam_num)}-->
											<!--{foreach from=$errors.message.exam_num item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<div class="info info3">
										<!--{html_radios name="pass_type" options=$form_elements.pass_type selected=$pass_type assign=pass_type}-->
										<span><!--{$pass_type.0}--><span class="blueNote">※1</span></span>
										<span><!--{$pass_type.1}--><span class="blueNote">※2</span></span>
										<!--{if isset($errors.list.pass_type)}-->
											<!--{foreach from=$errors.message.pass_type item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<p class="info4">
										※1 今回の官報にお名前が掲載された方。<br>
										※2 今回の官報にお名前が掲載されていないが、学位取得などの申請により5科目合格となる方。
									</p>
									<p class="info5">●該当する欄にチェックしてください。</p>
									<div class="subTable">
										<table>
											<tbody>
												<tr class="first">
													<th></th>
													<td>簿<br>記</td>
													<td>財<br>表</td>
													<td>法<br>人</td>
													<td>所<br>得</td>
													<td>相<br>続</td>
													<td>酒<br>税</td>
													<td>消<br>費</td>
													<td>固<br>定</td>
													<td>事<br>業</td>
													<td>住<br>民</td>
													<td>国<br>徴</td>
													<td>物<br>品</td>
												</tr>
												<tr class="examSub targetTr">
													<th>今回受験科目</th>
													<!--{html_checkboxes name="exam_sub" options=$form_elements.exam_sub selected=$exam_sub assign=exam_sub}-->
													<td><!--{$exam_sub.0}--></td>
													<td><!--{$exam_sub.1}--></td>
													<td><!--{$exam_sub.2}--></td>
													<td><!--{$exam_sub.3}--></td>
													<td><!--{$exam_sub.4}--></td>
													<td><!--{$exam_sub.5}--></td>
													<td><!--{$exam_sub.6}--></td>
													<td><!--{$exam_sub.7}--></td>
													<td><!--{$exam_sub.8}--></td>
													<td><!--{$exam_sub.9}--></td>
													<td><!--{$exam_sub.10}--></td>
													<td>-</td>
												</tr>
												<tr class="passSub targetTr">
													<th>今回合格科目</th>
													<!--{html_checkboxes name="pass_sub" options=$form_elements.pass_sub selected=$pass_sub assign=pass_sub}-->
													<td><!--{$pass_sub.0}--></td>
													<td><!--{$pass_sub.1}--></td>
													<td><!--{$pass_sub.2}--></td>
													<td><!--{$pass_sub.3}--></td>
													<td><!--{$pass_sub.4}--></td>
													<td><!--{$pass_sub.5}--></td>
													<td><!--{$pass_sub.6}--></td>
													<td><!--{$pass_sub.7}--></td>
													<td><!--{$pass_sub.8}--></td>
													<td><!--{$pass_sub.9}--></td>
													<td><!--{$pass_sub.10}--></td>
													<td>-</td>
												</tr>
												<tr class="passSubPast targetTr">
													<th>過年度に合格済の科目</th>
													<!--{html_checkboxes name="pass_sub_past" options=$form_elements.pass_sub_past selected=$pass_sub_past assign=pass_sub_past}-->
													<td><!--{$pass_sub_past.0}--></td>
													<td><!--{$pass_sub_past.1}--></td>
													<td><!--{$pass_sub_past.2}--></td>
													<td><!--{$pass_sub_past.3}--></td>
													<td><!--{$pass_sub_past.4}--></td>
													<td><!--{$pass_sub_past.5}--></td>
													<td><!--{$pass_sub_past.6}--></td>
													<td><!--{$pass_sub_past.7}--></td>
													<td><!--{$pass_sub_past.8}--></td>
													<td><!--{$pass_sub_past.9}--></td>
													<td><!--{$pass_sub_past.10}--></td>
													<td><!--{$pass_sub_past.11}--></td>
												</tr>
												<tr class="attendSub">
													<th>TAC受講歴のある科目</th>
													<!--{html_checkboxes name="attend_sub" options=$form_elements.attend_sub selected=$attend_sub assign=attend_sub}-->
													<td><!--{$attend_sub.0}--></td>
													<td><!--{$attend_sub.1}--></td>
													<td><!--{$attend_sub.2}--></td>
													<td><!--{$attend_sub.3}--></td>
													<td><!--{$attend_sub.4}--></td>
													<td><!--{$attend_sub.5}--></td>
													<td><!--{$attend_sub.6}--></td>
													<td><!--{$attend_sub.7}--></td>
													<td><!--{$attend_sub.8}--></td>
													<td><!--{$attend_sub.9}--></td>
													<td><!--{$attend_sub.10}--></td>
													<td>-</td>
												</tr>
											</tbody>
										</table>
										<!--{if isset($errors.list.exam_sub)}-->
											<!--{foreach from=$errors.message.exam_sub item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
										<!--{if isset($errors.list.pass_sub)}-->
											<!--{foreach from=$errors.message.pass_sub item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
										<!--{if isset($errors.list.pass_sub_past)}-->
											<!--{foreach from=$errors.message.pass_sub_past item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
										<!--{if isset($errors.list.attend_sub)}-->
											<!--{foreach from=$errors.message.attend_sub item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<div class="info info6">
										<p class="infoText">会計免除のある方</p>
										<!--{html_radios name="accounts_exemp" options=$form_elements.accounts_exemp selected=$accounts_exemp assign=accounts_exemp}-->
										<span><!--{$accounts_exemp.0}--></span>
										<span><!--{$accounts_exemp.1}--></span>
										<span><!--{$accounts_exemp.2}--></span>
										<!--{if isset($errors.list.accounts_exemp)}-->
											<!--{foreach from=$errors.message.accounts_exemp item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<div class="info info6">
										<p class="infoText">税法免除のある方</p>
										<!--{html_radios name="taxation_exemp" options=$form_elements.taxation_exemp selected=$taxation_exemp assign=taxation_exemp}-->
										<span><!--{$taxation_exemp.0}--></span>
										<span><!--{$taxation_exemp.1}--></span>
										<span><!--{$taxation_exemp.2}--></span>
										<!--{if isset($errors.list.taxation_exemp)}-->
											<!--{foreach from=$errors.message.taxation_exemp item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<div class="info info6">
										<p class="infoText2">何回目の受験で5科目合格されましたか？</p>
										<input type="text" name="exam_times" value="<!--{$exam_times|escape}-->">回
										<!--{if isset($errors.list.exam_times)}-->
											<!--{foreach from=$errors.message.exam_times item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr id="questionnaire">
								<th>アンケート</th>
								<td>
									<p class="text2">①TACを選んだ理由を教えてください(200文字以内)</p>
									<textarea name="reason" cols="5" rows="5"><!--{$reason|escape}--></textarea>
									<!--{if isset($errors.list.reason)}-->
										<!--{foreach from=$errors.message.reason item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<p class="text">②学習を開始した時点で、簿記の知識レベルはどれくらいでしたか？</p>
									<!--{html_options name="lebel" options=$form_elements.lebel selected=$lebel}-->
									<!--{if isset($errors.list.lebel)}-->
										<!--{foreach from=$errors.message.lebel item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<p class="text">③学習を開始した時はどのような状況でしたか？</p>
									<!--{html_options name="situation" options=$form_elements.situation selected=$situation}-->
									<!--{if isset($errors.list.situation)}-->
										<!--{foreach from=$errors.message.situation item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<p class="text">④最初に受験した科目は何ですか？</p>
									<!--{html_options name="first_sub" options=$form_elements.first_sub selected=$first_sub}-->
									<!--{if isset($errors.list.first_sub)}-->
										<!--{foreach from=$errors.message.first_sub item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
									<p class="text">⑤合格の秘訣を1つ挙げるとすれば何ですか？(200文字以内)</p>
									<textarea name="secret" cols="5" rows="5"><!--{$secret|escape}--></textarea>
									<!--{if isset($errors.list.secret)}-->
										<!--{foreach from=$errors.message.secret item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>
									5科目合格祝賀会<br>
									「集合写真」の<br>
									送付（無料）について
								</th>
								<td>
									<p class="ttl">「集合写真」の送付（無料）を希望しますか？</p>
									<div class="info info3">
										<!--{html_radios name="photo" options=$form_elements.photo selected=$photo assign=photo}-->
										<span><!--{$photo.0}--></span>
										<span><!--{$photo.1}--></span>
										<!--{if isset($errors.list.photo)}-->
											<!--{foreach from=$errors.message.photo item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<p class="text3">
										※送付希望の方は、下記に送付先をご記入ください。<br>
										記入漏れがあった場合は、写真はお送りいたしかねますのでご注意ください。
									</p>
									<p class="text4">集合写真送付記入欄<span class="blueNote">※建物名・部屋番号がある場合は必ずご記入ください。</span></p>
									<textarea class="textarea1" name="photo_address" cols="5" rows="5"><!--{$photo_address|escape}--></textarea>
									<!--{if isset($errors.list.photo_address)}-->
										<!--{foreach from=$errors.message.photo_address item="val"}-->
										<span class="error"><!--{$val}--></span>
										<!--{/foreach}-->
									<!--{/if}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>合格体験記について</th>
								<td>
									<p class="ttl">合格体験記の執筆にご協力いただけますか？<span>※謝礼あり</span></p>
									<div class="info info3">
										<!--{html_radios name="memoir" options=$form_elements.memoir selected=$memoir assign=memoir}-->
										<span><!--{$memoir.0}--></span>
										<span><!--{$memoir.1}--></span>
										<!--{if isset($errors.list.memoir)}-->
											<!--{foreach from=$errors.message.memoir item="val"}-->
											<span class="error"><!--{$val}--></span>
											<!--{/foreach}-->
										<!--{/if}-->
									</div>
									<p class="text3">※ご協力いただける方には、後日、TAC税理士講座事務局より執筆依頼のご連絡をさせていただきます。</p>
									<p class="text3">
										※ご執筆いただいた方には、謝礼といたしまして\5,000を指定の銀行口座へ<br>
										お振込みさせていただきます。
									</p>
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
						</tbody>
					</table>

					<div class="txtBox">
						<p class="ttl">TACと清文社の合弁会社【プロフェッションネットワーク】<br>プレミアム会員権2年間分（\32,400相当）無料プレゼント！</p>
						<p class="txt">
							見事、5科目合格を果たされた皆様には、【プロフェッションネットワーク】プレミアム会員サービスを<br>2年間分（通常年会費\16,200×2年間）無料プレゼントいたします。
						</p>
						<dl class="txtDl">
							<dt>プレミアム会員のサービスについて</dt>
							<dd>
								<ul class="txtList">
									<li>●法改正など実務で役立つ情報を素早く受け取れる<br>週刊Web情報誌『Profession Journal』を購読することができます。</li>
									<li>●TAC出版と実務書出版の清文社の書籍が特別割引価格で購入することができます。</li>
									<li>●税理士業務に役立つ実務セミナーを会員優待価格で受講することができます。</li>
								</ul>
							</dd>
						</dl>
						<div class="hope clearfix">
							<p>プレミアム会員への登録を希望しますか？</p>
							<!--{html_radios name="premium" options=$form_elements.premium selected=$premium assign=premium}-->
							<ul class="checkUl">
								<li><!--{$premium.0}--></li>
								<li><!--{$premium.1}--></li>
							</ul>
							<!--{if isset($errors.list.premium)}-->
								<!--{foreach from=$errors.message.premium item="val"}-->
								<span class="error"><!--{$val}--></span>
								<!--{/foreach}-->
							<!--{/if}-->
						</div>
						<ul class="txtUl">
							<li>※プレミアム会員サービスの利用期間は、サービス開始のお知らせメールが届いた日より2年間となります。</li>
							<li>※2年間のプレミアム会員期間経過後は、自動的に無料の「一般会員」に移行させていただきます。<br>そのまま、有料会員となることはございません。ご安心ください。</li>
						</ul>
					</div>

					<div class="itemBox">
						<p class="ttl">■ 祝賀会参加にあたっての確認事項</p>
						<ul class="itemUl">
							<li>
								1．<span>TAC税理士講座コース（オプション講座や全国公開模試を除く）を受講し、<br>第66回税理士試験において5科目合格を達成している方が参加対象者です。</span>
							</li>
							<li>2．回答いただいたアンケートにつきまして、パンフレットやTACホームページに掲載させていただく場合がございます。</li>
							<li>3．祝賀会当日の集合写真、個人写真、スナップ撮影、VTR撮影はTACで制作する広報物として使用させていただくことをご了承の上ご出席ください。</li>
							<li>4．男性の方はジャケット・ネクタイ着用</li>
						</ul>
						<p class="ttl">■ 個人情報の取り扱いにあたっての確認事項</p>
						<p class="txt">【個人情報の取扱いについて】</p>
						<ul class="itemUl">
							<li>1．事業者の名称　TAC株式会社　個人情報保護管理者　個人情報保護管理室　室長  連絡先　<a href="mailto:cpo@tac-school.co.jp">cpo@tac-school.co.jp</a></li>
							<li>2．ご提供いただいた個人情報（写真含む）は、合否の確認、パンフレット・TACホームページ等の広告物への掲載及び個人を特定しない統計情報として利用いたします。</li>
							<li>3．ご提供いただいた個人情報は、（株）プロフェッションネットワークのプレミアム会員への登録を希望された場合は、（株）プロフェッションネットワークの会員登録情報として利用いたします。</li>
							<li>4．ご提供いただいた個人情報は、お客様の同意なしに業務委託先以外の第三者に開示、提供することはありません（ただし、法令等により開示を求められた場合を除く）。</li>
							<li>5．ご提供いただいた個人情報の利用目的の通知、開示、訂正、削除、利用又は提供の停止等を請求することができます。下記の窓口までご相談ください。</li>
							<li>6．TACへの個人情報の提供は任意です。ただし、必要な個人情報がご提供いただけない場合等は、円滑なサービスのご提供に支障をきたす可能性があります。あらかじめご了承ください。</li>
						</ul>
						<p class="txt">個人情報に関する問合せ窓口　E-mail：<a href="mailto:privacy@tac-school.co.jp">privacy@tac-school.co.jp</a></p>
					</div>

					<p class="errorTxt">未入力の項目がございます。</p>
					<ul class="submit">
						<li>
							<input type="image" name="__submit__" src="../../images/zei/celebration_reserve/index/btn01.jpg" width="700" height="71" alt="上記確認事項・個人情報の取り扱いに同意の上 登録内容を確認する">
						</li>
					</ul>
				</form>
			</div>
			<p class="redNote redNote01">※予約確認メールが受信できない場合は、下記をご確認ください。</p>
			<p class="ttlTxt">迷惑メールフォルダやゴミ箱に振り分けされている</p>
			<p class="txt01">ご利用のセキュリティソフトやメールソフトに、迷惑メール防止機能が付いている場合、迷惑メールフォルダやゴミ箱に自動振り分けされたり、削除された可能性があります。迷惑メールフォルダ内やセキュリティソフトのメーカーに確認のうえ、［@tac-school.co.jp］のドメインを受信可能にしてください。携帯電話のご利用の場合、携帯電話以外からのメールは受信しないよう、標準設定されていることがあります。受信可能にするための設定方法は、ご利用の携帯会社・機種によって異なりますので携帯電話会社へお尋ねください。</p>
			<p class="ttlTxt">登録完了後、予約確認メールが届かず、ご予約内容が確認できない場合は、下記項目を入力の上、下記までお問合せくださいませ。</p>
			<p class="mailTxt">■TAC会員番号（0から始まる10桁の数字）　■氏名　■電話番号　■住所<br>お問合せ先　<a href="mailto:zeirishi-soudan@tac-school.co.jp">zeirishi-soudan@tac-school.co.jp</a></p>
		</section>
	</div>
	<!-- /#main -->

	<footer id="footer">
		<div class="fBox">
			<ul class="fNavi">
				<li><a href="http://www.tac-school.co.jp/" target="_blank">資格の学校TAC税理士講座</a> | </li>
				<li><a href="http://www.tac-school.co.jp/privacy.html" target="_blank">個人情報保護方針</a></li>
			</ul>
			<div class="flogo">
				<a href="https://seal.starfieldtech.com/verifySeal?sealID=0SvI0SgUeSBlVJfUQwatyZuH5T9UkI770cXh84uaOVdc6iXBMjn8ftxxLuu" target="_blank"><img src="../../images/zei/celebration_reserve/common/f_logo.png" width="128" height="56" alt="Starfield Trusted SSL Site"></a>
			</div>
			<small class="copyright">Copyright&copy;2011. TAC Co., Ltd. All Rights Reserved.br</small>
		</div>
	</footer>
</div>
</body>
</html>
