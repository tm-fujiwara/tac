<?php
///////////////////////////////////////////////////////////////////////////
/// config.phpの内容をオーバーライドする場合
/// （条件によって送信先を変えたい。バリデーションルールを変えたい。）
/// はここで記述すること（ソースが見難くならないように。）
/// フォームからのパラメータによるif分岐などは$_SESSIONを利用してください。
/// ex)
/// if (!empty($_SESSION['hoge'])) {
///  	add_valid_rule('estimate', 'required', 'お見積り内容は必須です。');
/// }
///////////////////////////////////////////////////////////////////////////
if(!empty($_POST['photo'])) {
	if($_POST['photo'] === '希望する'){
		add_valid_rule(array('photo_address'), 'required', '集合写真送付記入欄は必須項目です');
		// if(!empty($_POST['photo_address'])){
		// 	$_POST['photo_address'] = str_replace("\r\n", '', $_POST['photo_address']);
		// 	add_valid_rule(array('photo_address', 'photo_address_default'), 'different', '集合写真送付記入欄は必須項目です');
		// }
	}
}
