<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="TAC税理士講座「税理士試験5科目合格祝賀会」ご予約ページです。">
<meta name="keywords" content="税理士,試験,5科目,合格,祝賀会">
<title>「税理士試験5科目合格祝賀会」ご予約</title>
<link href="../../css/zei/celebration_reserve.css" rel="stylesheet">
<script src="../../js/zei/celebration_reserve/common.js"></script>
</head>
<body id="thanks">
<div id="container">
	<header id="header" class="clearfix">
		<h1><a href="http://www.tac-school.co.jp/" target="_blank"><img src="../../images/zei/celebration_reserve/common/logo.png" width="271" height="32" alt="資格の学校TAC"></a></h1>
		<div class="hTtl"><img src="../../images/zei/celebration_reserve/common/imgtext01.png" width="336" height="215" alt="参加対象者 TAC税理士講座のコース※を受講され66回税理a士試験において5科目合格を達成された方 ※オプション講座や全a国公開模試のみの受講者は対象外となります。"></div>
	</header>

	<div class="mainVisual">
		<section id="pageTtl">
			<h2 class="h2Ttl"><img src="../../images/zei/celebration_reserve/index/h2_img.png" width="472" height="209" alt="平成28年度(第66回)税理士試験 5科目合格祝賀会 Web予約受付 登録期日 各地区の祝賀会実施日の3日前まで"></h2>
		</section>
	</div>

	<div id="main">
		<section id="contact">
			<h3 class="contactH3">送信完了</h3>
			<p>送信が完了しました。
			後日メールにて祝賀会招待メールをお送りいたします。</p>
			<p class="redNote redNote01">※予約確認メールが受信できない場合は、下記をご確認ください。</p>
			<p class="ttlTxt">迷惑メールフォルダやゴミ箱に振り分けされている</p>
			<p class="txt01">ご利用のセキュリティソフトやメールソフトに、迷惑メール防止機能が付いている場合、迷惑メールフォルダやゴミ箱に自動振り分けされたり、削除された可能性があります。迷惑メールフォルダ内やセキュリティソフトのメーカーに確認のうえ、［@tac-school.co.jp］のドメインを受信可能にしてください。携帯電話のご利用の場合、携帯電話以外からのメールは受信しないよう、標準設定されていることがあります。受信可能にするための設定方法は、ご利用の携帯会社・機種によって異なりますので携帯電話会社へお尋ねください。</p>
			<p class="ttlTxt">登録完了後、予約確認メールが届かず、ご予約内容が確認できない場合は、下記項目を入力の上、下記までお問合せくださいませ。</p>
			<p class="mailTxt">■TAC会員番号（0から始まる10桁の数字）　■氏名　■電話番号　■住所<br>お問合せ先　<a href="mailto:zeirishi-soudan@tac-school.co.jp">zeirishi-soudan@tac-school.co.jp</a></p>
		</section>
	</div>
	<!-- /#main -->

	<footer id="footer">
		<div class="fBox">
			<ul class="fNavi">
				<li><a href="http://www.tac-school.co.jp/" target="_blank">資格の学校TAC税理士講座</a> | </li>
				<li><a href="http://www.tac-school.co.jp/privacy.html" target="_blank">個人情報保護方針</a></li>
			</ul>
			<div class="flogo">
				<a href="https://seal.starfieldtech.com/verifySeal?sealID=0SvI0SgUeSBlVJfUQwatyZuH5T9UkI770cXh84uaOVdc6iXBMjn8ftxxLuu" target="_blank"><img src="../../images/zei/celebration_reserve/common/f_logo.png" width="128" height="56" alt="Starfield Trusted SSL Site"></a>
			</div>
			<small class="copyright">Copyright&copy;2011. TAC Co., Ltd. All Rights Reserved.br</small>
		</div>
	</footer>
</div>
</body>
</html>
