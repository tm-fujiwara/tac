<?php
class Log
{
	var $instance = null;
	var $save_dir  = null;
	var $count_file = 'count.dat';
	var $save_file = 'contact.csv';
	function getInstance() {
		// for php4
		return new Log();
	}
	
	function savePost($session) {
		do {
			if (!is_array($session)) break;

			$handle = fopen($this->save_dir . '/' . $this->save_file, 'a');
			if (!$handle) break;
			if (!fputcsv($handle, array_values($session))) break;
			return true;
		} while(0);
		return false;
	}

	function saveCount() {
		$filepath = $this->save_dir . '/' . $this->count_file;
		$cnt = @file_get_contents($filepath);
		$handle = fopen($filepath, 'w');
		if ( $cnt !== FALSE) {
			$cnt++;
		} else {
			$cnt = 1;
		}
		flock($handle, LOCK_EX);
		fputs($handle, $cnt);
		flock($handle, LOCK_UN);
		fclose($handle);
		return $cnt;
	}

	/**
	 * ファイルポインタから行を取得し、CSVフィールドを処理する
	 * @param resource handle
	 * @param int length
	 * @param string delimiter
	 * @param string enclosure
	 * @return ファイルの終端に達した場合を含み、エラー時にFALSEを返します。
	 */
	function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
		$d = preg_quote($d);
		$e = preg_quote($e);
		$_line = "";
		while (($eof != true)and(!feof($handle))) {
			$_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
			$itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
			if ($itemcnt % 2 == 0) $eof = true;
		}
		$_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
		$_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
		preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
		$_csv_data = $_csv_matches[1];
		for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
			$_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
			$_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
		}
		return empty($_line) ? false : $_csv_data;
	}
}