<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="description" content="TAC税理士講座「税理士試験5科目合格祝賀会」ご予約ページです。">
<meta name="keywords" content="税理士,試験,5科目,合格,祝賀会">
<title>「税理士試験5科目合格祝賀会」ご予約</title>
<link href="../../css/zei/celebration_reserve.css" rel="stylesheet">
<script src="../../js/zei/celebration_reserve/common.js"></script>
</head>
<body id="confirm">
<div id="container">
	<header id="header" class="clearfix">
		<h1><a href="http://www.tac-school.co.jp/" target="_blank"><img src="../../images/zei/celebration_reserve/common/logo.png" width="271" height="32" alt="資格の学校TAC"></a></h1>
		<div class="hTtl"><img src="../../images/zei/celebration_reserve/common/imgtext01.png" width="336" height="215" alt="参加対象者 TAC税理士講座のコース※を受講され66回税理a士試験において5科目合格を達成された方 ※オプション講座や全a国公開模試のみの受講者は対象外となります。"></div>
	</header>

	<div class="mainVisual">
		<section id="pageTtl">
			<h2 class="h2Ttl"><img src="../../images/zei/celebration_reserve/index/h2_img.png" width="472" height="209" alt="平成28年度(第66回)税理士試験 5科目合格祝賀会 Web予約受付 登録期日 各地区の祝賀会実施日の3日前まで"></h2>
		</section>
	</div>

	<div id="main">
		<section id="contact">
			<h3 class="contactH3">登録内容のご確認</h3>
			<div class="contactBox">
				<form action="#contact" method="post" class="mailForm">
					<table class="formTable">
						<tbody>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>TAC会員番号（10桁）</th>
								<td>
									<!--{$num|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>氏名</th>
								<td>
									<!--{$sei|escape}--> <!--{$mei|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>フリガナ</th>
								<td>
									<!--{$sei_kana|escape}--> <!--{$mei_kana|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td>
									<!--{$tel|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>メールアドレス</th>
								<td>
									<!--{$email|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>メールアドレス（確認用）</th>
								<td>
									<!--{$email_confirm|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>祝賀会参加会場</th>
								<td>
									<!--{$cele_area|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>科目情報</th>
								<td>
									<div class="info info1">
										<p class="infoText">平成28年度第66回税理士試験受験地</p>
										<!--{$exam_area|escape}-->
									</div>
									<div class="info info2">
										<p class="infoText">平成28年度第66回税理士試験受験番号（5桁）</p>
										<!--{$exam_num|escape}-->
									</div>
									<div class="info info3">
										<!--{$pass_type|escape}-->
									</div>
									<div class="subTable">
										<table>
											<tbody>
												<tr class="examSub targetTr">
													<th>今回受験科目</th>
													<td>
														<!--{foreach from=$exam_sub item="val"}-->
														<!--{$val}--> 
														<!--{/foreach}-->
													</td>
												</tr>
												<tr class="passSub targetTr">
													<th>今回合格科目</th>
													<td>
														<!--{foreach from=$pass_sub item="val"}-->
														<!--{$val}--> 
														<!--{/foreach}-->
													</td>
												</tr>
												<tr class="passSubPast targetTr">
													<th>過年度に合格済の科目</th>
													<td>
														<!--{foreach from=$pass_sub_past item="val"}-->
														<!--{$val}--> 
														<!--{/foreach}-->
													</td>
												</tr>
												<tr class="attendSub">
													<th>TAC受講歴のある科目</th>
													<td>
														<!--{foreach from=$attend_sub item="val"}-->
														<!--{$val}--> 
														<!--{/foreach}-->
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="info info6">
										<p class="infoText">会計免除のある方</p>
										<!--{$accounts_exemp|escape}-->
									</div>
									<div class="info info6">
										<p class="infoText">税法免除のある方</p>
										<!--{$taxation_exemp|escape}-->
									</div>
									<div class="info info6">
										<p class="infoText2">何回目の受験で5科目合格されましたか？</p>
										<!--{$exam_times|escape}-->回
									</div>
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr id="questionnaire">
								<th>アンケート</th>
								<td>
									<p class="text2">①TACを選んだ理由を教えてください(200文字以内)</p>
									<!--{$reason|escape|nl2br}-->
									<p class="text">②学習を開始した時点で、簿記の知識レベルはどれくらいでしたか？</p>
									<!--{$lebel|escape}-->
									<p class="text">③学習を開始した時はどのような状況でしたか？</p>
									<!--{$situation|escape}-->
									<p class="text">④最初に受験した科目は何ですか？</p>
									<!--{$first_sub|escape}-->
									<p class="text">⑤合格の秘訣を1つ挙げるとすれば何ですか？(200文字以内)</p>
									<!--{$secret|escape|nl2br}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>
									5科目合格祝賀会<br>
									「集合写真」の<br>
									送付（無料）について
								</th>
								<td>
									<p class="ttl">「集合写真」の送付（無料）を希望しますか？</p>
									<!--{$photo|escape}-->
									<p class="text4">集合写真送付記入欄</p>
									<!--{$photo_address|escape|nl2br}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
							<tr>
								<th>合格体験記について</th>
								<td>
									<p class="ttl">合格体験記の執筆にご協力いただけますか？</p>
									<!--{$memoir|escape}-->
								</td>
							</tr>
							<tr class="dotTr">
								<td colspan="2"></td>
							</tr>
						</tbody>
					</table>

					<div class="txtBox">
						<div class="hope clearfix">
							<p>プレミアム会員への登録を希望しますか？</p>
						</div>
						<ul class="txtUl">
							<li><!--{$premium|escape}--></li>
						</ul>
					</div>

					<div class="itemBox">
						<p class="ttl">■ 祝賀会参加にあたっての確認事項</p>
						<ul class="itemUl">
							<li>
								1．<span>TAC税理士講座コース（オプション講座や全国公開模試を除く）を受講し、<br>第66回税理士試験において5科目合格を達成している方が参加対象者です。</span>
							</li>
							<li>2．回答いただいたアンケートにつきまして、パンフレットやTACホームページに掲載させていただく場合がございます。</li>
							<li>3．祝賀会当日の集合写真、個人写真、スナップ撮影、VTR撮影はTACで制作する広報物として使用させていただくことをご了承の上ご出席ください。</li>
							<li>4．男性の方はジャケット・ネクタイ着用</li>
						</ul>
						<p class="ttl">■ 個人情報の取り扱いにあたっての確認事項</p>
						<p class="txt">【個人情報の取扱いについて】</p>
						<ul class="itemUl">
							<li>1．事業者の名称　TAC株式会社　個人情報保護管理者　個人情報保護管理室　室長  連絡先　<a href="mailto:cpo@tac-school.co.jp">cpo@tac-school.co.jp</a></li>
							<li>2．ご提供いただいた個人情報（写真含む）は、合否の確認、パンフレット・TACホームページ等の広告物への掲載及び個人を特定しない統計情報として利用いたします。</li>
							<li>3．ご提供いただいた個人情報は、（株）プロフェッションネットワークのプレミアム会員への登録を希望された場合は、（株）プロフェッションネットワークの会員登録情報として利用いたします。</li>
							<li>4．ご提供いただいた個人情報は、お客様の同意なしに業務委託先以外の第三者に開示、提供することはありません（ただし、法令等により開示を求められた場合を除く）。</li>
							<li>5．ご提供いただいた個人情報の利用目的の通知、開示、訂正、削除、利用又は提供の停止等を請求することができます。下記の窓口までご相談ください。</li>
							<li>6．TACへの個人情報の提供は任意です。ただし、必要な個人情報がご提供いただけない場合等は、円滑なサービスのご提供に支障をきたす可能性があります。あらかじめご了承ください。</li>
						</ul>
						<p class="txt">個人情報に関する問合せ窓口　E-mail：<a href="mailto:privacy@tac-school.co.jp">privacy@tac-school.co.jp</a></p>
					</div>

					<ul class="submit">
						<li>
							<button type="submit" name="__retry_input__">修正する</button>
						</li>
						<li>
							<button type="submit" name="__send__">送信する</button>
						</li>
					</ul>
				</form>
			</div>
			<p class="redNote redNote01">※予約確認メールが受信できない場合は、下記をご確認ください。</p>
			<p class="ttlTxt">迷惑メールフォルダやゴミ箱に振り分けされている</p>
			<p class="txt01">ご利用のセキュリティソフトやメールソフトに、迷惑メール防止機能が付いている場合、迷惑メールフォルダやゴミ箱に自動振り分けされたり、削除された可能性があります。迷惑メールフォルダ内やセキュリティソフトのメーカーに確認のうえ、［@tac-school.co.jp］のドメインを受信可能にしてください。携帯電話のご利用の場合、携帯電話以外からのメールは受信しないよう、標準設定されていることがあります。受信可能にするための設定方法は、ご利用の携帯会社・機種によって異なりますので携帯電話会社へお尋ねください。</p>
			<p class="ttlTxt">登録完了後、予約確認メールが届かず、ご予約内容が確認できない場合は、下記項目を入力の上、下記までお問合せくださいませ。</p>
			<p class="mailTxt">■TAC会員番号（0から始まる10桁の数字）　■氏名　■電話番号　■住所<br>お問合せ先　<a href="mailto:zeirishi-soudan@tac-school.co.jp">zeirishi-soudan@tac-school.co.jp</a></p>
		</section>
	</div>
	<!-- /#main -->

	<footer id="footer">
		<div class="fBox">
			<ul class="fNavi">
				<li><a href="http://www.tac-school.co.jp/" target="_blank">資格の学校TAC税理士講座</a> | </li>
				<li><a href="http://www.tac-school.co.jp/privacy.html" target="_blank">個人情報保護方針</a></li>
			</ul>
			<div class="flogo">
				<a href="https://seal.starfieldtech.com/verifySeal?sealID=0SvI0SgUeSBlVJfUQwatyZuH5T9UkI770cXh84uaOVdc6iXBMjn8ftxxLuu" target="_blank"><img src="../../images/zei/celebration_reserve/common/f_logo.png" width="128" height="56" alt="Starfield Trusted SSL Site"></a>
			</div>
			<small class="copyright">Copyright&copy;2011. TAC Co., Ltd. All Rights Reserved.br</small>
		</div>
	</footer>
</div>
</body>
</html>
