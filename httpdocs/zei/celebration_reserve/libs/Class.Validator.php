<?php
class Validator
{
	var $error_count;
	var $error_results = array();
	var $error_list = array();
	var $error_messages = array();

	/**
	 * コンストラクタ
	 * 
	 */
	function Validator($params, $setting_list) {
		$this->init($params, $setting_list);
	}

	function init($params, $setting_list) {
		$this->parseParams($params, $setting_list);
	}

	/**
	 * バリデーション処理部
	 */
	function parseParams($params, $setting_list) {
		// 初期化
		$error_results = array();
		$error_messages = array();
		$error_results = array();

		// setting_listを回してparamから検証を行う
		foreach ($setting_list as $valid) {

			switch($valid['type']) {
				case 'equal':
				case 'different':
				case 'separatedEmail':
					// 比較する値2つを渡す
					$check_value = array($params[$valid['key'][0]], $params[$valid['key'][1]]);
					break;
				case 'maxlength':
				case 'minlength':
					// 対象のvalueと検証設定値を渡す
					$check_value = array($params[$valid['key'][0]], $valid['key'][1]);
					break;
				case 'tel':
				case 'zip':
				case 'required':
					$number = array();
					// 複数の入力欄で分割されていた場合にも対応
					foreach ($valid['key'] as $part) {
						$number[] = $params[$part];	
					}
					$check_value = $number;
					break;
				default:
					$check_value= $params[$valid['key'][0]];
					break;
			}
			$setkey = $valid['key'][0];
			$valid_bool = $this->checkIndividual($check_value, $valid['type']);
			$error_list[$setkey][$valid['type']] = $valid_bool;

			if ($valid_bool === true) {
				$error_results[$setkey] = true;
				$error_messages[$setkey][] = $valid['message'];
			}
			
		}
		// エラーカウントの取得
		$error_count = count($error_results);
		// プロパティへのセット
		$this->setErrorCount($error_count);
		$this->setErrorResults($error_results);
		$this->setErrorList($error_list);
		$this->setErrorMessages($error_messages);

	}


	/**
	 * チェックタイプによって振り分ける
	 */
	function checkIndividual($val, $check_type) {
		switch($check_type) {
			case 'required':
				$result = $this->isRequired($val);
				break;
			case 'email':
				$result = $this->isEmail($val);
				break;
			case 'separatedEmail':
				$result = $this->separatedEmail($val);
				break;
			case 'tel':
				$result = $this->isTel($val);
				break;
			case 'zip':
				$result = $this->isZip($val);
				break;
			case 'numeric':
				$result = $this->isNumeric($val);
				break;
			case 'alpha':
				$result = $this->isAlpha($val);
				break;
			case 'alnum':
				$result = $this->isAlnum($val);
				break;
			case 'url':
				$result = $this->isUrl($val);
				break;
			case 'maxlength':
				$result = $this->isMaxLength($val);
				break;
			case 'minlength':
				$result = $this->isMinLength($val);
				break;
			case 'equal':
				$result = $this->isEqual($val);
				break;
			case 'different':
				$result = $this->isDifferent($val);
				break;
			default:
				$result = true;
				break;
		}
		return $result;
	}
	
	/**
	 * 値が空かどうか検証する（中継用）
	 * @return true(空だった) : false(空ではなかった)
	 */
	 function emptyCheck($val) {
	 	return $this->isRequired($val);
	 }

	/**
	 * 値が空かどうかを検証する
	 */
	function isRequired($val) {
		if (is_array($val)) {
			foreach ($val as $v) {
				if (empty($v) && $v != '0') return true;
			}
			return false;
		} else {
			return (empty($val) && $val != '0') ? true : false;
		}
	}

	/**
	 * 値がメールアドレス形式かどうかを検証する
	 */
	function isEmail($val) {
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		return (!preg_match( '/^([*+!.&#$|\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})$/i', $val)) ? true : false;	
	}
	/**
	 * @で分断されたメールアドレス用のチェック
	 */
	function separatedEmail($val) {
		$val = implode($val, '@');
		return $this->isEmail($val);
	}
	/**
	 * 電話番号チェック
	 */
	function isTel($val) {
		if (is_array($val)) {
			$val = implode($val);
		}
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		// ハイフンはすべて取り除く
		$val = str_replace(array('ー','-'), '', $val);
		// 数字チェックを入れる
		if (Validator::isNumeric($val) === true) return true;
		// 先頭が0で始まり、かつ10桁または11桁かどうか		
		if ('0' === $val{0} && (strlen($val) == 10 || strlen($val) == 11)) {
			return false;
		}
		return true;
	}

	/**
	 * 郵便番号チェック
	 */
	function isZip($val) {
		if (is_array($val)) {
			$val = implode($val);
		}
		
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		// ハイフンはすべて取り除く
		$val = str_replace(array('ー','-'), '', $val);
		// 数字チェックを入れる
		if (Validator::isNumeric($val) === true) return true;
		// 7桁の数字かどうかチェック
		if (strlen($val) == 7) {
			return false;
		}
		return true;
	}

	/**
	 * 値が数字かどうか検証する
	 */
	function isNumeric($val) {
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		return (!is_numeric($val)) ? true : false;
	}
	
	/**
	 * 値が英字かどうか検証する
	 */
	function isAlpha($val) {
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		return (!ctype_alpha($val)) ? true : false;
	}
	
	/**
	 * 値が英数字かどうか検証する
	 */
	function isAlnum($val) {
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		return (!ctype_alnum($val)) ? true : false;
	}
	
	function isUrl($val) {
		// 値が空の場合は検証しない
		if ($this->emptyCheck($val)) return false;
		return (!preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $val)) ? true : false;	
	}
	
	/**
	 * 文字数MAXチェック
	 * $val[0] : 検証する値
	 * $val[1] : 最大文字数の指定
	 */
	function isMaxLength($val) {
		return (mb_strlen($val[0]) <= $val[1]) ? false : true;
	}
	
	/**
	 * 文字数MINチェック
	 * $val[0] : 検証する値
	 * $val[1] : 最小文字数の指定
	 */
	function isMinLength($val) {
		return (mb_strlen($val[0]) >= $val[1]) ? false : true;
	}
	
	/**
	 * 2つの値が同一の値かどうかを確認する
	 */
	function isEqual($val) {
		return ($val[0] != $val[1]) ? true : false;
	}

	/**
	 * 2つの値が同一の値ではないかどうかを確認する
	 */
	function isDifferent($val) {
		return ($val[0] == $val[1]) ? true : false;
	}
	
////////////////////////////////////////////////////////////////////////////
/////// アクセサメソッド
////////////////////////////////////////////////////////////////////////////
	/**
	 * エラーカウント
	 */
	function setErrorCount($error_count) {
		$this->error_count = $error_count;
	}

	function getErrorCount() {
		return $this->error_count;
	}
	/**
	 * エラーメッセージ
	 */
	function setErrorMessages($error_messages) {
		$this->error_messages = $error_messages;
	}

	function getErrorMessages() {
		return $this->error_messages;
	}
	/**
	 * エラーリザルト（項目がエラーだったかどうかをtrue or falseで）
	 */
	function setErrorResults($error_results) {
		$this->error_results = $error_results;
	}

	function getErrorResults() {
		return $this->error_results;
	}
	/**
	 * エラーリスト（項目内で何がエラーだったのかをtrue or falseで）
	 */
	function setErrorList($error_list) {
		$this->error_list = $error_list;
	}

	function getErrorList() {
		return $this->error_list;
	}
} // end validator class

